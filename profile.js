import { success, failure } from './libs/response-lib';
import * as dynamoDbLib from "./libs/dynamodb-lib";
import merge from 'merge';

export async function setProfile(event, context) {
  const data = JSON.parse(event.body);

  if (! (data.accountName && data.profile)) {
    return failure({ message: "accountName is required"});
  }

  try {

    let contactRecord = await dynamoDbLib.getOrCreateByEOSAccount(data.accountName);
    console.log(" Contact Record from getOrCreateByEOSAccount: ", contactRecord);

    if (contactRecord.profile) {
      contactRecord.profile = merge.recursive(true, contactRecord.profile, data.profile);
    } else {
      contactRecord.profile = data.profile;
    }

    console.log(" Contact Record before saveContact  : ", contactRecord);

    await dynamoDbLib.saveContact(contactRecord);
    return success({
        status: true,
        message: `Profile record saved successfully`
    });
  } catch (e) {
      console.log(" ERROR  : ", e);
      return failure({ message: e.message });
  }
}


export async function getProfile(event, context) {

    try {
        let accountName = event.pathParameters.accountName;
        const accountRecord = await dynamoDbLib.getByEOSAccount(accountName);

        console.log(' accountRecord: ', accountRecord);

        return success({ profile: accountRecord.profile });
    } catch (e) {
        return failure({ message: e.message });
    }
}
