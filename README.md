# Hypha Services

Simple web service for saving and retrieving a profile data object

Endpoint: /setprofile

profile JSON object can include any data. 
When setting JSON after the initial time, it recusively merges the JSON data. Existing attributes will be updated and new attributes added.

Example Data
```
{
	"accountName":"tester",
	"profile": {
		"nickname": "johnson",
		"description": "My name is johnson and this is my profile.",
		"avatarLink": "https://someimagesite.com/myprofileavatar"
	}
}
```

Endpoint: /getprofile/{accountName}

Return JSON data object


