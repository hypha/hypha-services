import { success, failure } from './libs/response-lib';
import * as dynamoDbLib from "./libs/dynamodb-lib";

export async function main(event, context) {

    try {
        let accountName = event.pathParameters.accountName;
        const accountRecord = await dynamoDbLib.getByEOSAccount(accountName);

        console.log(' accountRecord: ', accountRecord);

        return success({ profile: accountRecord.profile });
    } catch (e) {
        return failure({ message: e.message });
    }
}
