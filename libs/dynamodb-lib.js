import AWS from "aws-sdk";
import uuid from "uuid";
AWS.config.update({ region: 'us-east-1' });

export function call(action, params) {

    const dynamoDb = new AWS.DynamoDB.DocumentClient();
    return dynamoDb[action](params).promise();
}

export async function saveContact(contactRecord) {
    contactRecord.updatedAt = Date.now();
    await call("put", {
        TableName: process.env.tableName,
        Item: contactRecord
    });
}

export async function findByEOSAccount(accountName) {
    const readParams = {
        TableName: process.env.tableName,
        IndexName: 'GSI_accountName',
        KeyConditionExpression: 'accountName = :accountName',
           ExpressionAttributeValues: {
            ':accountName': accountName
        }
    };

    const { Count, Items } = await call("query", readParams);
    return Count > 0 ? Items[0] : null;
}


export async function getByEOSAccount(accountName) {
    const contact = await findByEOSAccount(accountName);
    if (!contact) {
        throw new Error(`EOS Account ${accountName} not found`);
    }
    return contact;
}

export async function getOrCreateByEOSAccount(accountName) {
    let contactRecord = await findByEOSAccount(accountName);
    console.log("  findByEOSAccount: Query result  : ", contactRecord);
    if (!contactRecord) {
        contactRecord = {
            contactKey: uuid.v1(),
            accountName: accountName,
            createdAt: Date.now()
        };
        console.log(" Contact not found. Creating new: ", contactRecord);
    }
    return contactRecord;
}

export async function getByContactKey(contactKey) {
    const readParams = {
        TableName: process.env.tableName,
        Key: {
            contactKey: contactKey
        }
    };

    const result = await call("get", readParams);
    if (!result.Item) {
        throw new Error(`Contact Key ${contactKey} not found`);
    }

    return result.Item;
}