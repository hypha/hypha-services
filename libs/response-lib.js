export function success(body) {
    body.success = "true";
    return buildResponse(200, body);
}

export function failure(body) {
    body.success = "false";
    return buildResponse(500, body);
}

function buildResponse(statusCode, body) {
    return {
        statusCode: statusCode,
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true
        },
        body: JSON.stringify(body)
    };
}
