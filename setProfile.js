import { success, failure } from './libs/response-lib';
import * as dynamoDbLib from "./libs/dynamodb-lib";
import merge from 'merge';

export async function main(event, context) {
  const data = JSON.parse(event.body);

  if (! (data.accountName && data.profile)) {
    return failure({ message: "accountName is required"});
  }

  try {

    let contactRecord = await dynamoDbLib.getOrCreateByEOSAccount(data.accountName);
    console.log(" Contact Record from getOrCreateByEOSAccount: ", contactRecord);

    if (contactRecord.profile) {
      contactRecord.profile = merge.recursive(true, contactRecord.profile, data.profile);
    } else {
      contactRecord.profile = data.profile;
    }

    console.log(" Contact Record before saveContact  : ", contactRecord);

    await dynamoDbLib.saveContact(contactRecord);
    return success({
        status: true,
        message: `Profile record saved successfully`
    });
  } catch (e) {
      console.log(" ERROR  : ", e);
      return failure({ message: e.message });
  }
}
